This script contain important functions which help in reducing human workload. And also helps beginners to get started with python. Code documentation is aligned correctly when the files are viewed in Notepad++. Jarvis is used as a google assistant. master

batch_file_rename.py - This batch renames a group of files in a given directory, once you pass the current and the new extensions.

create_dir_if_not_there.py - Checks to see if a directory exists in the users home directory. If a directory does not exist, then one will be created.

Fast Youtube Downloader - Downloads YouTube videos quickly with parallel threads using aria2c.

Google Image Downloader - Query a given term and retrieve images from the Google Image database.

dir_test.py - Tests to see if the directory testdir exists, if not it will create the directory for you if you want it created.

env_check.py - This script will check to see if all of the environment variables required are set.

blackjack.py - This script contains the Casino BlackJack-21 Game in Python.

fileinfo.py - Shows file information for a given file.

folder_size.py - Scans the current directory and all subdirectories and displays the size.

logs.py - This script will search for all *.log files in the given directory, zip them using the program you specify, and then date stamp them.

move_files_over_x_days.py - Moves all files over a specified age (in days) from the source directory to the destination directory. perfect

nslookup_check.py - This simple script opens the file server_list.txt and then does a nslookup for each one to check the DNS entry.

osinfo.py - Displays some information about the OS on which you are running this script.

ping_servers.py - This script, depending on the arguments supplied, will ping the servers associated with that application group.

ping_subnet.py - After supplying the first 3 octets this file scans the final range for available addresses.

powerdown_startup.py - This file goes through the server list and pings the machine, if it is up it will load the putty session, if it is not then it will notify you.

puttylogs.py - This file zips up all the logs in the given directory.

script_count.py - This file scans the scripts directory and gives a count of the different types of scripts.

[get_youtube_view.py] - This is a simple python script used to get more views on your YouTube videos. This script may also be used to repeat songs on YouTube.

script_listing.py - This file will list all the files in the given directory, and go through all the subdirectories as well.

testlines.py - This simple script opens a file and prints out 100 lines of whatever is the set for the line variable.

tweeter.py - Allows you to tweet text or a picture from the terminal.

serial_scanner.py contains a method called ListAvailablePorts which returns a list with the names of the serial ports that are in use in the computer. This method works only on Linux and Windows (can be extended for mac OS). If no port is found, an empty list is returned.

get_youtube_view.py - A simple python script to get more views for your YouTube videos. Useful for repeating songs on YouTube.

CountMillionCharacter.py And CountMillionCharacter2.0.py - Gets character count of a text file.

xkcd_downloader.py - Downloads the latest XKCD comic and places them in a new folder called "comics".

timymodule.py - A great alternative to Python 'timeit' module and easier to use.

calculator.py - Uses Python's eval() function to implement a calculator.

Google_News.py - Uses BeautifulSoup to provide Latest news headline along with news link.

cricket_live_score - Uses BeautifulSoup to provide live cricket score.

youtube.py - It Takes a song name as input and fetches the YouTube URL of the best matching song and plays it.

site_health.py - Checks the health of a remote server

SimpleStopWatch.py - Simple Stop Watch implementation using Python's time module.

Changemac.py - This script change your MAC address , generate random MAC address or enter input as new MAC address in your Linux(Successfully Tested in Ubuntu 18.04).

whatsapp-monitor.py - Uses Selenium to give online status about your contacts when your contacts become online in WA you will get an update about it on terminal.

whatsapp-chat-analyzer.py - This is Whatsapp group/individual chat analyzer . This script is able to analyze all activity happened in Whatsapp group and visualize all things through matplotlib library(In Graph form).

JARVIS.py - Control windows programs with your voice.

Images Downloader - Download Image Form webpage Work on Unix based systems.

space_invader.py.py - Classical space invader 2D game.
Recall your old childhood memories, by playing the classic space invader game.

Test Case Generator - Generates different types of test cases with a clean and friendly UI, used in competitive programming and potentially for software testing.
